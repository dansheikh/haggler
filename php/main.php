<?php
namespace Haggler;

require(dirname(__FILE__).'/controllers/UsersController.php');
require(dirname(__FILE__).'/controllers/SessionsController.php');
require(dirname(__FILE__).'/controllers/AuctionsController.php');
require(dirname(__FILE__).'/controllers/CategoriesController.php');
require(dirname(__FILE__).'/controllers/ItemsController.php');
require(dirname(__FILE__).'/controllers/RatingsController.php');
require(dirname(__FILE__).'/controllers/ObserversController.php');
require(dirname(__FILE__).'/controllers/BidsController.php');
require(dirname(__FILE__).'/controllers/DashboardController.php');
require(dirname(__FILE__).'/controllers/NotificationsController.php');
require(dirname(__FILE__).'/controllers/RecommendationsController.php');

use Haggler\Controllers;

// Instantiate controllers.
$usersController = new Controllers\UsersController();
$sessionsController = new Controllers\SessionsController();
$auctionsController = new Controllers\AuctionsController();
$categoriesController = new Controllers\CategoriesController();
$itemsController = new Controllers\ItemsController();
$ratingsController = new Controllers\RatingsController();
$observersController = new Controllers\ObserversController();
$bidsController = new Controllers\BidsController();
$dashboardController = new Controllers\DashboardController();
$notificationsController = new Controllers\NotificationsController();
$recommendationsController = new Controllers\RecommendationsController();

$headers = apache_request_headers();

$routeTable = array('users' => $usersController,
                    'sessions' => $sessionsController,
                    'auctions' => $auctionsController,
                    'categories' => $categoriesController,
                    'items' => $itemsController,
                    'ratings'=> $ratingsController,
                    'observers'=> $observersController,
                    'bids'=> $bidsController,
                    'dashboard' => $dashboardController,
                    'notifications' => $notificationsController, 
                    'recommendations' => $recommendationsController);

// Get target path.
$url = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];

list($controller, $action) = explode('/', strtok(substr($url, 1), '?'));

// Check mode & route.
$exclude = array('register', 'signin', 'signout');
if (getenv('HAGGLER_MODE') == 'pro' && !in_array($action, $exclude)) {

    if ($headers['session-token'] == NULL) {
        header('Content-Type: application/json', true, 401);
        echo json_encode(array('error' => 'Session token is required.'));
        exit;
    } else {
         $db = new \mysqli('p:127.0.0.1', 'barginhunter', 'thrifty', 'Haggler');
         $stmt = $db->prepare("SELECT * from Sessions WHERE id = ?");
         $stmt->bind_param('s', $session_token);
         $session_token = $headers['session-token'];
         $result = $stmt->execute();
         $stmt->bind_result($sess_id);
         $stmt->fetch();

         if (!$result || $sess_id === NULL) {
            header('Content-Type: application/json', true, 401);
            echo json_encode(array('error' => 'Unable to validate session token.'));
         }
    }
}

if ($routeTable[$controller] !== NULL) {
    call_user_func(array($routeTable[$controller], $action), $method, $headers, $_REQUEST);
}
?>
