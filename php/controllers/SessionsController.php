<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class SessionsController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function signin($method, $headers, $request) {
        /*
            example request object:
            {
                "email" : "jon.doe@me.com",
                "password" : "testing123"
            }
            response:
            {
                "session_token": "11bd4f8c-c285-4b53-a9c5-5f7a01d66c60",
                "user_id": "efbd4f8c-c685-4z53-a9c5-5f7a01d66c10",
                "first_name": "Jon",
                "last_name": "Doe",
                "type": 1
            }
        */
        if ($method === 'POST') {
            $json = file_get_contents('php://input');
            $data = json_decode($json, true);
            
            // Validate credentials.
            $user_stmt = $this->db->prepare("SELECT id, password, username, first_name, last_name, type FROM Users WHERE email = ?"); 
            $user_stmt->bind_param('s', $user_email);
            $user_email = $data['email'];
            $user_stmt->execute();
            $user_stmt->bind_result($user_id, $password, $username, $first_name, $last_name, $type);
            $user_stmt->fetch();

            if ($password !== hash('sha256', $data['password'])) {
                header('Content-Type: application/json', true, 401);
                echo json_encode(array('error' => 'Unable to validate credentials.', 'errtype' => 'session', 'errno' => null));
                exit;
            }

            $user_stmt->close();

            $stmt = $this->db->prepare("INSERT INTO Sessions(id, user_id) VALUES(?, ?)");
            $stmt->bind_param('ss', $id, $user_id);

            $id = BaseController::generate_uuid();

            $stmt->execute();
            $stmt->close();
            $this->db->close();
            
            header('Content-Type: application/json', true, 201);
            echo json_encode(array('session_token' => $id, 'user_id' => $user_id, 'username' => $username, 'first_name' => $first_name, 'last_name' => $last_name, 'type' => $type));
        } else {
            BaseController::bad_request($method, $request);
        }
    }

    public function signout($method, $headers, $request) {
        if ($method === 'GET') {
            $stmt = $this->db->prepare("DELETE FROM Sessions WHERE id = ?");
            $stmt->bind_param('s', $session_token);
            $session_token = $headers['session-token'];
            if ($stmt->execute() && $this->db->affected_rows > 0) {
                $stmt->close();
                $this->db->close();

                echo header('Content-Type: application/json', true, 204);
            } else {
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => "Unable to delete session $session_token", 'errtype' => 'session', 'errno' => $stmt->errno));            
            }
        } else {
            BaseController:bad_request($method, $request);
        }
    }

}
?>
