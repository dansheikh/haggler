<?php
namespace Haggler\Controllers;

class BaseController {
    protected $db;
    protected static $LIMIT = 10;

    public function __construct() {
        $db_config = file_get_contents(dirname(__FILE__).'/../configurations/db.json');
        $db_json = json_decode($db_config, true);
        $this->db = new \mysqli($db_json['host'], $db_json['user'], $db_json['password'], $db_json['db']);

        if ($this->db->connect_errno > 0) {
            die("Unable to connect to database: $this->db->connect_error");
        }   
    }

    public static function generate_uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', 
                        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
                        mt_rand( 0, 0xffff ),
                        mt_rand( 0, 0x0fff ) | 0x4000,
                        mt_rand( 0, 0x3fff ) | 0x8000,
                        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ));
    }

    public static function bad_request($name, $args) {
        header('Content-Type: application/json', true, 400);
        echo json_encode(array('action' => $name, 'args' => $args));
    }

    public static function offset($page) {
        return self::$LIMIT * ($page - 1);
    }

    public function __call($name, $args) {
        header('Content-Type: application/json', true, 404);
        echo json_encode(array('action' => $name, 'args' => $args));
    }
}

?>
