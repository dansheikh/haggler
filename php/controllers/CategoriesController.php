<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class CategoriesController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

   public function index($method, $headers, $request) {
        if ($method === 'GET') {
            $stmt = $this->db->prepare("SELECT id, title FROM Categories");
            
            if ($stmt->execute()) {
                $stmt->bind_result($id, $title);
                $categories = array();

                while ($stmt->fetch()) {
                    $row = array('id' => $id, 'title' => $title);
                    array_push($categories, $row);
                }

                $stmt->close();
                $this->db->close();

                header('Content-Type: application/json', true, 200);
                echo json_encode(array('categories' => $categories));
            } else {
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => "Unable to retrieve categories", 'errtype' => 'categories', 'errno' => $stmt->errno));            
            }
        } else {
            BaseController:bad_request($method, $request);
        }
    }

}
?>
