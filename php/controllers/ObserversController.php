<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class ObserversController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function create($method, $headers, $request) {

        /*
            POST request
            eg request object: (get user ID from the session token in the header)
            {
                "auction_id" : "2d08f6e1-94c3-48a7-a7a8-e8d79dce39f4",
                "stamp": "my stamp"
            }
        */

        // CHECK USER TYPE
        // uses session token info from header to check whether corresponding user is a buyer or a seller.
        // buyer === 0, seller === 1, and combined === 2

        $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

        $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
        $user_stmt = $this->db->prepare("SELECT id, type FROM Users WHERE id = ?"); 
        
        $sess_stmt->bind_param('s', $sess_id);
        $user_stmt->bind_param('s', $user_key);
        
        $sess_id = $headers['session-token'];
        
        $sess_stmt->execute();
        $sess_stmt->bind_result($sess_id_db, $user_id);
        $sess_stmt->fetch();
        $sess_stmt->close();

        $user_key = $user_id;

        $user_stmt->execute();
        $user_stmt->bind_result($user_tbl_id, $user_type);
        $user_stmt->fetch();
        $user_stmt->close();

        if($sess_id_db === null || $user_type == 1){
            header('Content-Type: application/json', true, 401);
            echo json_encode(array('error' => 'Unauthorised to watch auction.', 'errtype' => 'auction'));
        }


        if ($method === 'POST') {
            if ($user_type==1){
                // If user type is 1 then they are only a seller - so they cannot watch any auctions
                echo header('Content-Type: application/json', true, 403);
                echo json_encode(array('Error' => 'Only potential buyers can watch auctions'));
            } else {
                $json = file_get_contents('php://input');
                $data = json_decode($json, true);

                $stmt = $this->db->prepare("INSERT INTO Observers(buyer_id, auction_id, stamp) VALUES(?, ?, ?)");
                $stmt->bind_param('sss', $user_key, $auction_id, $stamp);
                
                $auction_id = $this->db->escape_string($data['auction_id']);
                $stamp = $this->db->escape_string($data['stamp']);

                $result = $stmt->execute();
                $stmt->close();
                $this->db->close();


                if(!$result){
                    echo header('Content-Type: application/json', true, 409);
                    echo json_encode(array('error' => 'database query error'));
                }else{
                    echo header('Content-Type: application/json', true, 201);
                }
            }  
        } else {
            BaseController::bad_request($method, $request);
        }

    }

    public function delete($method, $headers, $request) {
        /*
            eg request object (use user ID from session-token in the header):
            {
                "auction_id" : "SOME AUCTION",
            }
        */

        // CHECK USER TYPE
        // uses session token info from header to check whether corresponding user is a buyer or a seller.
        // buyer === 0, seller === 1, and combined === 2

        $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

        $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
        $user_stmt = $this->db->prepare("SELECT id, type FROM Users WHERE id = ?"); 
        
        $sess_stmt->bind_param('s', $sess_id);
        $user_stmt->bind_param('s', $user_key);
        
        $sess_id = $headers['session-token'];
        
        $sess_stmt->execute();
        $sess_stmt->bind_result($sess_id_db, $user_id);
        $sess_stmt->fetch();
        $sess_stmt->close();

        $user_key = $user_id;

        $user_stmt->execute();
        $user_stmt->bind_result($user_tbl_id, $user_type);
        $user_stmt->fetch();
        $user_stmt->close();


        if ($method === 'POST') {
            $json = file_get_contents('php://input');
            $data = json_decode($json, true);

            $stmt = $this->db->prepare("DELETE FROM Observers WHERE buyer_id = ? AND auction_id = ?");
            $stmt->bind_param('ss', $user_key, $auction_id);
            
            $auction_id = $this->db->escape_string($data['auction_id']);
            
            $result = $stmt->execute();
            $stmt->close();
            $this->db->close();
            echo header('Content-Type: application/json', true, 769);


            if(!$result){
                echo header('Content-Type: application/json', true, 409);
                echo json_encode(array('error' => 'database query error'));
            }else{
                echo header('Content-Type: application/json', true, 201);
            }
        } else {
            BaseController::bad_request($method, $request);
        }
    }
}

?>
