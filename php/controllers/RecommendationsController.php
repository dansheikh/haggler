<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class RecommendationsController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    /*
        eg request object: GET request call (put the session-token in the header)
        http://localhost:8888/recommendations/show?q=2452241

        Sample response object:
        {
            "recommendations": [
            {
              "auction_id": "24522411",
              "starting_price": "12.31",
              "description": "Very cool iPhone",
              "image": "https://support.apple.com/library/content/dam/edam/applecare/images/en_US/iphone/iphone-iphone5s-colors.jpg"
            },
            {
              "auction_id": "2452242",
              "starting_price": "20.00",
              "description": "Very cool iPhone",
              "image": "https://support.apple.com/library/content/dam/edam/applecare/images/en_US/iphone/iphone-iphone5s-colors.jpg"
            },
            {
              "auction_id": "2452249",
              "starting_price": "12.31",
              "description": "Very cool iPhone",
              "image": "https://support.apple.com/library/content/dam/edam/applecare/images/en_US/iphone/iphone-iphone5s-colors.jpg"
            },
            {
              "auction_id": "24522410",
              "starting_price": "20.00",
              "description": "Very cool iPhone",
              "image": "https://support.apple.com/library/content/dam/edam/applecare/images/en_US/iphone/iphone-iphone5s-colors.jpg"
            },
            {
              "auction_id": "2452243",
              "starting_price": "300.00",
              "description": "Very cool iPhone",
              "image": "https://support.apple.com/library/content/dam/edam/applecare/images/en_US/iphone/iphone-iphone5s-colors.jpg"
            }
            ]
        }
     */
    public function show($method, $headers, $request) {      
         if ($method === 'GET') {
            $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);    

            $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
            $sess_stmt->bind_param('s', $sess_id);
            $sess_id = $headers['session-token'];
            $sess_stmt->execute();
            $sess_stmt->bind_result($sess_id_db, $user_id);
            $sess_stmt->fetch();
            $sess_stmt->close();

            if($sess_id_db === null){
                header('Content-Type: application/json', true, 401);
                echo json_encode(array('error' => 'Unauthorised session token.', 'errtype' => 'recommendations', 'errno' => null));
            }

            $q = array_key_exists('q', $request) ? $request['q'] : null;
            
            if ($q === null) {
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => 'Basis for recommendations is required.', 'errtype' => 'recommendations', 'errno' => null));
                return;
            }

            $basis = $this->db->escape_string($q);
            $rec_stmt = $this->db->prepare("SELECT DISTINCT a.id, seller.username AS seller, sqagg.avg_score, c.title AS category, i.description, i.image, a.starting_price, a.reserve_price, a.expiration, a.stamp FROM Bids b JOIN Auctions a ON b.auction_id = a.id JOIN Categories c ON a.category_id = c.id JOIN Items i ON a.item_id = i.id JOIN Users seller ON a.seller_id = seller.id LEFT JOIN (SELECT AVG(r.score) AS avg_score, r.recipient_id AS recipient_id FROM Ratings r GROUP BY r.recipient_id) sqagg ON a.seller_id = sqagg.recipient_id WHERE b.buyer_id IN (SELECT DISTINCT b.buyer_id FROM Bids b WHERE b.auction_id = ? AND a.expiration > NOW() AND b.buyer_id <> ?) AND b.auction_id <> ? LIMIT 5");
            $rec_stmt->bind_param('sss', $q, $user_id, $q);
            $rec_stmt->bind_result($id, $seller, $avg_score, $category, $description, $image, $starting_price, $reserve_price, $expiration, $stamp);
            $auctions = array();   
            $id_list = array();     

            if ($rec_stmt->execute()) {
                while ($rec_stmt->fetch()) {
                    $row = array('id' => $id, 'category' => $category, 'description' => stripslashes($description), 'image' => $image, 'starting_price' => $starting_price, 'reserve_price' => $reserve_price, 'expiration' => $expiration, 'stamp' => $stamp, 'seller' => $seller, 'avg_score' => $avg_score);
                    array_push($auctions, $row);
                    $id_list[$id] = true;   
                }           
            } else {
                $res_code = $rec_stmt->errno < 2000 ? 400 : 500;
                header('Content-Type: application/json', true, $res_code);
                echo json_encode(array('error' => $rec_stmt->error, 'errtype' => 'recommendations', 'errno' => $rec_stmt->errno));
                $rec_stmt->close();
                $this->db->close();
                return;
            }

            if (count($auctions) === 5) {
                header('Content-Type: application/json', true, 200);
                echo json_encode(array('recommendations' => $auctions), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);                
                $rec_stmt->close();
                $this->db->close();
                return;
            } else {
                $limit = (5 - count($auctions)) * 2;

                $sup_stmt = $this->db->prepare("SELECT a.id, c.title, i.description, i.image, a.starting_price, a.reserve_price, a.expiration, a.stamp, u.username as seller, sq.avg_score FROM Auctions a JOIN Categories c ON a.category_id = c.id JOIN Items i ON a.item_id = i.id JOIN Users u ON a.seller_id = u.id LEFT JOIN (SELECT AVG(r.score) AS avg_score, r.recipient_id AS recipient_id FROM Ratings r GROUP BY r.recipient_id) sq ON a.seller_id = sq.recipient_id WHERE a.expiration > NOW() ORDER BY a.stamp DESC LIMIT ?"); 
                $sup_stmt->bind_param('s', $limit);            

                if ($sup_stmt->execute()) {
                    $sup_stmt->bind_result($id, $category, $description, $image, $starting_price, $reserve_price, $expiration, $stamp, $seller, $avg_score);

                    while ($sup_stmt->fetch() && count($auctions) < 5) {
                        $row = array('id' => $id, 'category' => $category, 'description' => stripslashes($description), 'image' => $image, 'starting_price' => $starting_price, 'reserve_price' => $reserve_price, 'expiration' => $expiration, 'stamp' => $stamp, 'seller' => $seller, 'avg_score' => $avg_score);
                        
                        if (!array_key_exists($id, $id_list))
                            array_push($auctions, $row);
                    }
        
                    header('Content-Type: application/json', true, 200);
                    echo json_encode(array('recommendations' => $auctions), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);                
                    $sup_stmt->close();
                    $this->db->close();
                    return;
                } else {
                    $res_code = $sup_stmt->errno < 2000 ? 400 : 500;
                    header('Content-Type: application/json', true, $res_code);
                    echo json_encode(array('error' => $sup_stmt->error, 'errtype' => 'recommendations', 'errno' => $sup_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);                
                    $sup_stmt->close();
                    $this->db->close();
                    return;
                }
            }
        } else {
            BaseController::bad_request($method, $request);
        }
    }

}
?>
