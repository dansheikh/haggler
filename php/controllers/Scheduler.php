<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class Scheduler extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    //interval for cronjob in mins
    var $interval = 5;
    
    public function run(){
     
        $auc_stmt = $this->db->prepare("SELECT auc.id,auc.seller_id,auc.reserve_price,Bids.buyer_id,Bids.value FROM Auctions auc LEFT JOIN Bids ON Bids.id = (SELECT Bids.id FROM Bids WHERE auc.id=Bids.auction_id ORDER BY Bids.value DESC LIMIT 1) WHERE auc.expiration BETWEEN DATE_SUB(NOW(),INTERVAL ? MINUTE) AND NOW()");
        $auc_stmt->bind_param('s', $this->interval);
        $auc_stmt->bind_result($id, $seller_id, $reserve_price, $buyer_id, $value);
        $auc_result=$auc_stmt->execute();


        if ($auc_result) {
            $auctions = array();

            while ($auc_stmt->fetch()) {
                $row = array('id' => $id, 'seller_id' => $seller_id, 'reserve_price' => $reserve_price, 'buyer_id' => $buyer_id, 'value' => $value);
                array_push($auctions, $row);
            }

        } else {                
            error_log($auc_stmt->error . ' notification_cron_script ' . $auc_stmt->errno);
            $auc_stmt->close();
            return;
        }

        
        foreach ($auctions as $auction) {
            print_r($auction);
            if($auction["buyer_id"] === null || $auction["value"] === null){
                
                //item was not sold, inform seller

                $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                $id = BaseController::generate_uuid();
                $user_id = $auction["seller_id"];
                $auction_id = $auction["id"];
                $type = -1; //auction_expired (without winner)
                $seen = 0;

                if (!$notify_stmt->execute()) {
                    $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                    $this->db->rollback();

                    error_log($notify_stmt->error . ' notification_cron_script ' . $notify_stmt->errno);

                    $notify_stmt->close();
                    return;
                }


            }else if($auction["value"] < $auction["reserve_price"]){

                //item was not sold, inform seller

                $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                $id = BaseController::generate_uuid();
                $user_id = $auction["seller_id"];
                $auction_id = $auction["id"];
                $type = -1; //auction_expired (without winner)
                $seen = 0;

                if (!$notify_stmt->execute()) {
                    $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                    $this->db->rollback();

                    error_log($notify_stmt->error . ' notification_cron_script ' . $notify_stmt->errno);

                    $notify_stmt->close();
                    return;
                }

                
                //item was not sold, inform observers on this auction

                $observers_stmt = $this->db->prepare("SELECT buyer_id FROM Observers WHERE Observers.auction_id=?");
                $observers_stmt->bind_param('s', $auction_id);
                $auction_id = $auction["id"];

                $observers_stmt->bind_result($buyer_id);
                $observers_result=$observers_stmt->execute();


                if ($observers_result) {
                    $observers = array();

                    while ($observers_stmt->fetch()) {
                        $row = array('buyer_id' => $buyer_id);
                        array_push($observers, $row);
                    }

                    foreach ($observers as $observer) {
                        $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                        $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                        $id = BaseController::generate_uuid();
                        $user_id = $observer['buyer_id'];
                        $auction_id = $auction["id"];
                        $type = -2; //auction_expired (without winner) inform bidder
                        $seen = 0;

                        if (!$notify_stmt->execute()) {
                            $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                            $this->db->rollback();

                            error_log($notify_stmt->error . ' notification_cron_script ' . $notify_stmt->errno);

                            $notify_stmt->close();
                            return;
                        }
                    }


                } else {                
                    error_log($observers_stmt->error . ' notification_cron_script ' . $observers_stmt->errno);
                    $observers_stmt->close();
                    return;
                }

                


                
            }else{
                
                //inform buyer of purchase
                $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                $id = BaseController::generate_uuid();
                $user_id = $auction["buyer_id"];
                $auction_id = $auction["id"];
                $type = 1; //purchase
                $seen = 0;
                        
                if (!$notify_stmt->execute()) {
                    $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                    $this->db->rollback();

                    error_log($notify_stmt->error . ' notification_cron_script ' . $notify_stmt->errno);

                    $notify_stmt->close();
                    return;
                }

                //inform buyer that payment successfully processed
                $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                $id = BaseController::generate_uuid();
                $user_id = $auction["buyer_id"];
                $auction_id = $auction["id"];
                $type = 3; //payment
                $seen = 0;
                        
                if (!$notify_stmt->execute()) {
                    $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                    $this->db->rollback();

                    error_log($notify_stmt->error . ' notification_cron_script ' . $notify_stmt->errno);

                    $notify_stmt->close();
                    return;
                }



                //inform seller of sale
                $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                $id = BaseController::generate_uuid();
                $user_id = $auction["seller_id"];
                $auction_id = $auction["id"];
                $type = 2; //sale
                $seen = 0;
                        
                if (!$notify_stmt->execute()) {
                    $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                    $this->db->rollback();

                    error_log($notify_stmt->error . ' notification_cron_script ' . $notify_stmt->errno);

                    $notify_stmt->close();
                    return;
                }

                //inform seller that payment successfully processed
                $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                $id = BaseController::generate_uuid();
                $user_id = $auction["seller_id"];
                $auction_id = $auction["id"];
                $type = 3; //payment
                $seen = 0;
                        
                if (!$notify_stmt->execute()) {
                    $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                    $this->db->rollback();

                    error_log($notify_stmt->error . ' notification_cron_script ' . $notify_stmt->errno);

                    $notify_stmt->close();
                    return;
                }


                //inform other bidders that they've lost this auction
                $observers_stmt = $this->db->prepare("SELECT buyer_id FROM Observers WHERE Observers.auction_id=? AND Observers.buyer_id<>?");
                $observers_stmt->bind_param('ss', $auction_id, $negate_user_id);
                $auction_id = $auction["id"];
                $negate_user_id = $auction["buyer_id"];

                $observers_stmt->bind_result($buyer_id);
                $observers_result=$observers_stmt->execute();


                if ($observers_result) {
                    $observers = array();

                    while ($observers_stmt->fetch()) {
                        $row = array('buyer_id' => $buyer_id);
                        array_push($observers, $row);
                    }

                    foreach ($observers as $observer) {
                        $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                        $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                        $id = BaseController::generate_uuid();
                        $user_id = $observer['buyer_id'];
                        $auction_id = $auction["id"];
                        $type = -2; //auction_expired (with winner in this case), inform bidder
                        $seen = 0;

                        if (!$notify_stmt->execute()) {
                            $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                            $this->db->rollback();

                            error_log($notify_stmt->error . ' notification_cron_script ' . $notify_stmt->errno);

                            $notify_stmt->close();
                            return;
                        }
                    }


                } else {                
                    error_log($observers_stmt->error . ' notification_cron_script ' . $observers_stmt->errno);
                    $observers_stmt->close();
                    return;
                }

            }
        }
        $this->db->close();
    }
}

$scheduler = new Scheduler;
$cron = $scheduler->run();

?>
