<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class NotificationsController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function index($method, $headers, $request) {
        if ($method === 'GET') {
            $pg = array_key_exists('pg', $request) ? $request['pg'] : 1;

            // CHECK USER INFO
            // uses session token info from header to check corresponding user information.
            $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
            $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
            $sess_stmt->bind_param('s', $sess_id);
            $sess_id = $headers['session-token'];
            $sess_stmt->execute();
            $sess_stmt->bind_result($sess_id_db, $user_id);
            $sess_stmt->fetch();
            $sess_stmt->close();


            if($sess_id_db === null || $user_id === null){
                header('Content-Type: application/json', true, 401);
                echo json_encode(array('error' => 'Unauthorised', 'errtype' => 'notifications', 'errno' => null));
                $this->db->close();
                return;
            }
            // END CHECK USER INFO

            $notify_cnt_stmt = $this->db->prepare("SELECT COUNT(id) FROM Notifications WHERE user_id=?");
            $notify_cnt_stmt->bind_param('s', $user_id);

            $notify_cnt_stmt->bind_result($notify_cnt);
            $notify_cnt_res = $notify_cnt_stmt->execute();
            $notify_cnt_stmt->fetch();

            if ($notify_cnt_res && $notify_cnt === 0) {
                header('Content-Type: application/json', true, 200);
                echo json_encode(array('notifications' => array(), 'count' => $notify_cnt, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            } else if (!$notify_cnt_res) {
                $res_code = $notify_cnt_res->errno < 2000 ? 400 : 500;
                header('Content-Type: application/json', true, $res_code);
                echo json_encode(array('error' => $notify_cnt_stmt->error, 'errtype' => 'notifications', 'errno' => $notify_cnt_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);                
            } else {
                $notify_cnt_stmt->close();
                $notify_stmt = $this->db->prepare("SELECT id, auction_id, type, stamp, seen FROM Notifications WHERE Notifications.user_id=? ORDER BY stamp DESC LIMIT ? OFFSET ?"); 
                $notify_stmt->bind_param('sss', $user_id, self::$LIMIT, BaseController::offset($pg));
                $notify_result = $notify_stmt->execute();

                if ($notify_result) {
                    $notify_stmt->bind_result($notify_id, $notify_auction_id, $notify_type, $notify_stamp, $notify_seen);
                    $notifications = array();
                    $note_ids = array();

                    while ($notify_stmt->fetch()) {
                        $row = array('id' => $notify_id, 'auction_id' => $notify_auction_id, 'type' => $notify_type, 'stamp' => $notify_stamp, 'seen' => $notify_seen);
                        array_push($notifications, $row);
                        array_push($note_ids, $notify_id);                    
                    }
                            
                    $sql = "UPDATE Notifications SET seen = 1 WHERE id IN ";
                    for ($i = 0; $i < count($note_ids); $i++) {
                        if ($i === 0 && count($note_ids) === 1)
                            $sql = $sql . "('{$note_ids[$i]}')";
                        else if ($i === 0 && count($note_ids) > 1)
                            $sql = $sql . "('{$note_ids[$i]}'";
                        else if ($i === count($note_ids) - 1)
                            $sql = $sql . ", '{$note_ids[$i]}')";
                        else
                            $sql = $sql . ", '{$note_ids[$i]}'";
                    }                    

                    if ($this->db->query($sql) === False)
                        error_log("error: " . $this->db->error);

                    header('Content-Type: application/json', true, 200);
                    echo json_encode(array('notifications' => $notifications,'count' => $notify_cnt, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

                    
                } else {
                    $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                    header('Content-Type: application/json', true, $res_code);
                    echo json_encode(array('error' => $notify_stmt->error, 'errtype' => 'notifications', 'errno' => $notify_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                }

                $notify_stmt->close();
                $this->db->commit();
                $this->db->close();
            }

        } else {
            BaseController::bad_request($method, $request);
        }  
    }    

}
?>
