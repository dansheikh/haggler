<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class BidsController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function create($method, $headers, $request) {
        /*
            eg request object:
            {
                "auction_id":"984826d0-9c3d-4b7b-acc2-d84b1cf25d77",
                "bid_offer":5.00            
            }
            
        */

        if ($method === 'POST') { 
            $json = file_get_contents('php://input');
            $data = json_decode($json, true);

            
            // CHECK USER TYPE
            // uses session token info from header to check whether corresponding user is a buyer or a seller.
            // buyer === 0, seller === 1, and combined === 2
            $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

            $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
            $user_stmt = $this->db->prepare("SELECT id, type FROM Users WHERE id = ?"); 

            
            $sess_stmt->bind_param('s', $sess_id);
            $user_stmt->bind_param('s', $user_key);
            
            $sess_id = $headers['session-token'];
            
            $sess_stmt->execute();
            $sess_stmt->bind_result($sess_id_db, $user_id);
            $sess_stmt->fetch();
            $sess_stmt->close();

            $user_key = $user_id;

            $user_stmt->execute();
            $user_stmt->bind_result($user_tbl_id, $user_type);
            $user_stmt->fetch();
            $user_stmt->close();


            if($sess_id_db === null || $user_key === null || $user_type === 1){
                header('Content-Type: application/json', true, 401);
                echo json_encode(array('error' => 'Unauthorised to create bid.', 'errtype' => 'bid', 'errno' => null));
                $this->db->close();
                return;
            }
            // END CHECK USER TYPE

            
            // If user is both buyer and seller (type 2), Check if they are trying to create a bid on their own auction
            $seller_auc_stmt = $this->db->prepare("SELECT seller_id, expiration, starting_price FROM Auctions WHERE Auctions.id=?"); 
            $seller_auc_stmt->bind_param('s', $auction_id);
            $auction_id = $this->db->escape_string($data['auction_id']);

            
            $starting_price_glob = 0;
            if($seller_auc_stmt->execute()){
                $seller_auc_stmt->bind_result($seller_id, $expiration, $starting_price);
                $seller_auc_stmt->fetch();
                $seller_auc_stmt->close();

                $starting_price_glob = $starting_price;
                
                    if($user_type === 2 && $seller_id == $user_key){
                        header('Content-Type: application/json', true, 400);
                        echo json_encode(array('error' => 'Unauthorised to bid on own auction', 'errtype' => 'bid', 'errno' => null));  
                        $this->db->close();
                        return;
                    }
                    if($expiration !== null){
                        $expiration_php = strtotime($expiration);
                        $curtime = time();
                        if($curtime > $expiration_php){
                            header('Content-Type: application/json', true, 400);
                            echo json_encode(array('error' => 'Auction has expired', 'errtype' => 'bid', 'errno' => null));  
                            $this->db->close();
                            return;
                        }
                    }else{
                        header('Content-Type: application/json', true, 400);
                        echo json_encode(array('error' => 'Unable to retrieve auction details', 'errtype' => 'bid', 'errno' => null));  
                        $this->db->close();
                        return;
                    }
                
            }else{
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => array('Unable to retrieve auction details',$seller_auc_stmt->error), 'errtype' => 'bid', 'errno' => $seller_auc_stmt->errno));  
                $this->db->close();
                return; 
            }
            // End check if seller is trying to create a bid on their own auction

            // Check if bid is greater than current highest bid
            $highestbid_stmt = $this->db->prepare("SELECT value FROM Bids, Auctions WHERE Bids.auction_id=Auctions.id AND Auctions.id=? ORDER BY value DESC LIMIT 1"); 
            $highestbid_stmt->bind_param('s', $auction_id);
            $auction_id = $this->db->escape_string($data['auction_id']);
            $value = $this->db->escape_string($data['bid_offer']);


            if($highestbid_stmt->execute()){
                $highestbid_stmt->bind_result($highest_bid_value);
                $highestbid_stmt->fetch();
                $highestbid_stmt->close();

                if($highest_bid_value !== null && $value <= $highest_bid_value){
                    header('Content-Type: application/json', true, 400);
                    echo json_encode(array('error' => 'Bid value is not higher than current highest bidder.', 'errtype' => 'bid', 'errno' => null));
                    $this->db->close();
                    return;
                }

                //Check that if no bids exist for auction, make sure first bid is >= starting price
                if($value < $starting_price_glob){
                    header('Content-Type: application/json', true, 400);
                    echo json_encode(array('error' => 'Bid value is not greater than or equal to starting price.', 'errtype' => 'bid', 'errno' => null));  
                    $this->db->close();
                    return;
                }
                //End starting bid value check

            }else{
                header('Content-Type: application/json', true, 500);
                echo json_encode(array('error' => 'Unable to retrieve current highest bidder.', 'errtype' => 'bid', 'errno' => null));   
                $this->db->close();
                return;
            }
            // End highest bid check

            $bid_stmt = $this->db->prepare("INSERT INTO Bids(id, buyer_id, auction_id, value) VALUES(?, ?, ?, ?)");
            $bid_stmt->bind_param('sssd', $id, $buyer_id, $auction_id, $value);

            $id = BaseController::generate_uuid();
            $buyer_id = $this->db->escape_string($user_key);
            $auction_id = $this->db->escape_string($data['auction_id']);
            $value = $this->db->escape_string($data['bid_offer']);
                    
            if (!$bid_stmt->execute()) {
                $res_code = $bid_stmt->errno < 2000 ? 400 : 500;
                $this->db->rollback();

                header('Content-Type: application/json', true, $res_code);
                echo json_encode(array('error' => $bid_stmt->error, 'errtype' => 'bid', 'errno' => $bid_stmt->errno));

                $bid_stmt->close();
                return;
            }else {
                //SEND OUTBID NOTIFICATIONS TO OTHER BIDDERS ON THIS AUCTION BY EMAIL

                // Add to observers table.
                $obs_stmt = $this->db->prepare("INSERT INTO Observers(buyer_id, auction_id) VALUES(?, ?)");
                $obs_stmt->bind_param('ss', $buyer_id, $auction_id);
                $obs_stmt->execute();

                $otherbidders_stmt = $this->db->prepare("SELECT Users.email, Users.id FROM Observers,Users,Auctions WHERE Observers.buyer_id=Users.id AND Observers.auction_id=Auctions.id AND Users.id<>? AND Auctions.id=?"); 
                $otherbidders_stmt->bind_param('ss', $user_id, $auction_id);
                $user_id = $user_key;
                $auction_id = $this->db->escape_string($data['auction_id']);

                $other_bidders_ids = array();                

                if($otherbidders_result=$otherbidders_stmt->execute()){

                    $otherbidders_stmt->bind_result($email, $other_user_id);
                    
                    $other_bidders_emails = array();                

                    while ($otherbidders_stmt->fetch()) {
                        array_push($other_bidders_emails, $email);
                        array_push($other_bidders_ids, $other_user_id);
                    }
                    $otherbidders_stmt->close();                    

                    //(comment out the line below to disable outgoing email)
                    if(!empty($other_bidders_emails)){                        
                        $msg = "You've been outbid in an auction!\nPlease sign into Haggler.com for more details.\n\nSincerely,\nHaggler Auction Team";
                        $this->authSendEmail($other_bidders_emails,'Outbid Alert', $msg);
                    } 


                }else{
                    error_log("Unable to notify other users of outbid via email.");
                }

                //SEND OUTBID NOTIFICATIONS TO OTHER BIDDERS ON THIS AUCTION ON THE NOTIFICATIONS TABLE
                foreach ($other_bidders_ids as $other_user_id) {
                    $notify_stmt = $this->db->prepare("INSERT INTO Notifications(id, user_id, auction_id, type, seen) VALUES(?, ?, ?, ?, ?)");
                    $notify_stmt->bind_param('sssdd', $id, $user_id, $auction_id, $type, $seen);

                    $id = BaseController::generate_uuid();
                    $user_id = $other_user_id;
                    $auction_id = $this->db->escape_string($data['auction_id']);
                    $type = 0; //outbid
                    $seen = 0;

                    if (!$notify_stmt->execute()) {
                        $res_code = $notify_stmt->errno < 2000 ? 400 : 500;
                        $this->db->rollback();

                        error_log($notify_stmt->error . ' bidscontroller ' . $notify_stmt->errno);
                        error_log("Unable to notify other users of outbid via notifs table.");

                        $notify_stmt->close();
                        
                    }
                }

                //END OUTBID NOTIFICATIONS

                $this->db->commit();
                echo header('Content-Type: application/json', true, 204);
            }

            $this->db->close();
        } else {
            BaseController::bad_request($method, $request);
        }
    }

    public function index($method, $headers, $request) {
        if ($method === 'GET') {

            // CHECK USER TYPE
            // uses session token info from header to check whether corresponding user is a buyer or a seller.
            // buyer === 0, seller === 1, and combined === 2
            $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

            $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
            
            $sess_stmt->bind_param('s', $sess_id);
            
            $sess_id = $headers['session-token'];
            
            $sess_stmt->execute();
            $sess_stmt->bind_result($sess_id_db, $user_id);
            $sess_stmt->fetch();
            $sess_stmt->close();

            $user_key = $user_id;


            if($sess_id_db === null || $user_key === null){
                header('Content-Type: application/json', true, 401);
                echo json_encode(array('error' => 'Unauthorised to create auction.', 'errtype' => 'bid','errno' => null));
                return;
            }
            // END CHECK USER TYPE


            $bid_stmt = $this->db->prepare("SELECT Bids.id,buyer_id,value,Bids.stamp FROM Bids, Auctions WHERE Bids.auction_id=Auctions.id AND Auctions.id=? AND Auctions.seller_id=? ORDER BY stamp DESC"); 
            $bid_stmt->bind_param('ss', $auction_id, $seller_id);
            $seller_id = $user_key;
            $auction_id = $this->db->escape_string($_GET['auction_id']);

            

            if($bid_stmt->execute()){

                $bid_stmt->bind_result($id, $buyer_id, $value, $stamp);

                $bids = array();

                while ($bid_stmt->fetch()) {
                    $row = array('id' => $id, 'value' => $value, 'stamp' => $stamp);
                    array_push($bids, $row);
                }
                $bid_stmt->close();
                $this->db->close();
                header('Content-Type: application/json', true, 200);
                echo json_encode(array('data' => $bids));

            }else{
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => array('Unable to get bid index ', $bid_stmt->error), 'errtype' => 'bid', 'errno' => $bid_stmt->errno));  
                return; 
            }
            
        } else {
            BaseController::bad_request($method, $request);
        }  
    }

    

    //
    // Adapted from phpBB2 (http://www.phpbb.com).
    //
    private function smtp_mail($recipients, $subject, $message, $headers = ''){

        $email_config = file_get_contents(dirname(__FILE__).'/../configurations/email.json');
        $email_json = json_decode($email_config, true);

        $user = $email_json['email'];
        $pass = $email_json['password'];
        $smtp_host = $email_json['smtp_host'];
        $port = $email_json['port'];
     
        $socket = fsockopen($smtp_host, $port, $errno, $errstr, 10);

        if (!$socket)
        {
          error_log("Error connecting to '$smtp_host' ($errno) ($errstr)");
        }else{
     
            $this->server_parse($socket, '220');
         
            fwrite($socket, 'EHLO '.$smtp_host."\r\n");
            $this->server_parse($socket, '250');
         
            fwrite($socket, 'AUTH LOGIN'."\r\n");
            $this->server_parse($socket, '334');
         
            fwrite($socket, base64_encode($user)."\r\n");
            $this->server_parse($socket, '334');
         
            fwrite($socket, base64_encode($pass)."\r\n");
            $this->server_parse($socket, '235');
         
            fwrite($socket, 'MAIL FROM: <'.$user.'>'."\r\n");
            $this->server_parse($socket, '250');
         
            foreach ($recipients as $email)
            {
                fwrite($socket, 'RCPT TO: <'.$email.'>'."\r\n");
                $this->server_parse($socket, '250');
            }
         
            fwrite($socket, 'DATA'."\r\n");
            $this->server_parse($socket, '354');
         
            fwrite($socket, 'Subject: '
              .$subject."\r\n".'To: <'.implode('>, <', $recipients).'>'
              ."\r\n".$headers."\r\n\r\n".$message."\r\n");
         
            fwrite($socket, '.'."\r\n");
            $this->server_parse($socket, '250');
         
            fwrite($socket, 'QUIT'."\r\n");
            fclose($socket);
         
            return true;
        }
    }
     
    //Function to Processes Server Response Codes
    private function server_parse($socket, $expected_response){
        $server_response = '';
        while (substr($server_response, 3, 1) != ' '){
            if (!($server_response = fgets($socket, 256))){
                error_log('Error while fetching server response codes.' . __FILE__ . __LINE__);
            }            
        }
     
        if (!(substr($server_response, 0, 3) == $expected_response)){
            error_log('Unable to send e-mail."'.$server_response.'"' . __FILE__ . __LINE__);
        }
    }


    private function authSendEmail($to, $subject, $message){

        if($this->smtp_mail($to, $subject, $message)){ //(to, subject, message)
            //echo "Email Sent Successfully.";
        }else{
            error_log("Error sending email.");
        }
    }


}
?>
