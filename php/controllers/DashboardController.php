<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class DashboardController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function index($method, $headers, $request) {
        if ($method === 'GET') {
            $q = array_key_exists('q', $request) ? $request['q'] : null;
            $pg = array_key_exists('pg', $request) ? $request['pg'] : 1;

            if ($q === null) {
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => 'Type query required.', 'errtype' => 'dashboard', 'errno' => null), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                return;
            }

            // CHECK USER TYPE
            // uses session token info from header to check whether corresponding user is a buyer or a seller.
            // buyer === 0, seller === 1, and combined === 2
            $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

            $sess_id = $headers['session-token'];

            $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
            $user_stmt = $this->db->prepare("SELECT id, type FROM Users WHERE id = ?"); 
            
            $sess_stmt->bind_param('s', $sess_id);
            $sess_stmt->bind_result($sess_id_db, $user_id);

            $user_stmt->bind_param('s', $user_key);
            $user_stmt->bind_result($user_tbl_id, $user_type);
                        
            if ($sess_stmt->execute()) {
                $sess_stmt->fetch();                
            } else {
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => $sess_stmt->error, 'errtype' => 'dashboard', 'errno' => $sess_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                return;
            }            

            $sess_stmt->close();

            $user_key = $user_id;

            if ($user_stmt->execute()) {
                $user_stmt->fetch();
            } else {
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => $user_stmt->error, 'errtype' => 'dashboard', 'errno' => $user_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                return;                
            }
            
            $user_stmt->close();

            $dash_cnt_stmt = null;
            $dash_stmt = null;

            // Check purchase and/or sale authorization.
            if ($user_type !== 1 && $q === 'purchases') {
                $dash_cnt_stmt = $this->db->prepare("SELECT COUNT(id) FROM Auctions a JOIN (SELECT b.auction_id, b.buyer_id, MAX(b.value) AS highest_bid FROM Bids b WHERE b.buyer_id = ? GROUP BY b.auction_id) sq ON a.id = sq.auction_id WHERE sq.highest_bid >= a.reserve_price AND a.expiration < NOW()");
                $dash_cnt_stmt->bind_param('s', $user_id);
                $dash_cnt_stmt->bind_result($dash_cnt);

                if ($dash_cnt_stmt->execute()) {
                    $dash_cnt_stmt->fetch();    
                } else {
                    $res_code = $auc_cnt_stmt->errno < 2000 ? 400 : 500;

                    header('Content-Type: application/json', true, $res_code);
                    echo json_encode(array('error' => $dash_cnt_stmt->error, 'errtype' => 'dashboard', 'errno' => $dash_cnt_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                    return;                    
                }

                $dash_cnt_stmt->close();

                if ($dash_cnt === 0) {
                    header('Content-Type: application/json', true, 200);
                    echo json_encode(array('purchases' => array(), 'count' => $dash_cnt, 'q' => $q, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);                    
                } else {
                    // Retrieve purchase data.
                    $dash_stmt = $this->db->prepare("SELECT a.id, u.username, a.seller_id, c.title, i.description, i.image, a.starting_price, a.reserve_price, a.expiration, a.stamp, sq.highest_bid, r.recipient_id, r.score, r.feedback FROM Auctions a JOIN (SELECT b.auction_id, b.buyer_id, MAX(b.value) AS highest_bid FROM Bids b WHERE b.buyer_id = ? GROUP BY b.auction_id) sq ON a.id = sq.auction_id JOIN Users u ON a.seller_id = u.id JOIN Categories c ON a.category_id = c.id JOIN Items i ON a.item_id = i.id LEFT JOIN Ratings r ON a.id = r.auction_id AND a.seller_id = r.recipient_id WHERE sq.highest_bid >= a.reserve_price AND a.expiration < NOW() ORDER BY a.expiration DESC LIMIT ? OFFSET ?");

                    $dash_stmt->bind_param('sss', $user_id, self::$LIMIT, BaseController::offset($pg));

                    if ($dash_stmt->execute()) {
                        $dash_stmt->bind_result();    
                        $dash_stmt->bind_result($id, $username, $seller_id, $category, $description, $image, $starting_price, $reserve_price, $expiration, $stamp, $highest_bid, $recipient_id, $rating_score, $rating_feedback);

                        $auctions = array();

                        while ($dash_stmt->fetch()) {
                            $row = array('id' => $id, 'seller' => $username, 'seller_id' => $seller_id, 'category' => $category, 'description' => stripslashes($description), 'image' => $image, 'starting_price' => $starting_price, 'reserve_price' => $reserve_price, 'expiration' => $expiration, 'stamp' => $stamp, 'highest_bid' => $highest_bid, 'recipient_id' => $recipient_id, 'rating_score' => $rating_score, 'rating_feedback' => $rating_feedback);
                            array_push($auctions, $row);
                        }
            
                        header('Content-Type: application/json', true, 200);
                        echo json_encode(array('auctions' => $auctions,'count' => $dash_cnt, 'q' => $q, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                    } else {
                        $res_code = $auc_cnt_stmt->errno < 2000 ? 400 : 500;

                        header('Content-Type: application/json', true, $res_code);
                        echo json_encode(array('error' => $dash_stmt->error, 'errtype' => 'dashboard', 'errno' => $dash_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                    }                    

                    $dash_stmt->close();
                }                                
            } else if ($user_type !== 0 && $q === 'sales') {
                $dash_cnt_stmt = $this->db->prepare("SELECT COUNT(id) FROM Auctions a LEFT JOIN (SELECT b.auction_id, b.buyer_id, MAX(b.value) AS highest_bid FROM Bids b GROUP BY b.auction_id) sq ON a.id = sq.auction_id WHERE a.seller_id = ?");
                $dash_cnt_stmt->bind_param('s', $user_id);                
                $dash_cnt_stmt->bind_result($dash_cnt);

                if ($dash_cnt_stmt->execute()) {
                    $dash_cnt_stmt->fetch();    
                } else {
                    $res_code = $auc_cnt_stmt->errno < 2000 ? 400 : 500;

                    header('Content-Type: application/json', true, $res_code);
                    echo json_encode(array('error' => $dash_cnt_stmt->error, 'errtype' => 'dashboard', 'errno' => $dash_cnt_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                    return;                    
                }

                $dash_cnt_stmt->close();

                if ($dash_cnt === 0) {
                    header('Content-Type: application/json', true, 200);
                    echo json_encode(array('purchases' => array(), 'count' => $dash_cnt, 'q' => $q, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);                    
                } else {
                    // Retrieve sale data.
                    $dash_stmt = $this->db->prepare("SELECT a.id, u.username, sq.buyer_id, c.title, i.description, i.image, a.starting_price, a.reserve_price, a.expiration, a.stamp, sq.highest_bid, r.recipient_id, r.score, r.feedback FROM Auctions a LEFT JOIN (SELECT b.auction_id, b.buyer_id, MAX(b.value) AS highest_bid FROM Bids b GROUP BY b.auction_id) sq ON a.id = sq.auction_id LEFT JOIN Users u ON sq.buyer_id = u.id JOIN Categories c ON a.category_id = c.id JOIN Items i ON a.item_id = i.id LEFT JOIN Ratings r ON a.id = r.auction_id AND a.seller_id = r.rater_id WHERE a.seller_id = ? ORDER BY a.stamp DESC LIMIT ? OFFSET ?");

                    $dash_stmt->bind_param('sss', $user_id, self::$LIMIT, BaseController::offset($pg));

                    if ($dash_stmt->execute()) {
                        $dash_stmt->bind_result();    
                        $dash_stmt->bind_result($id, $username, $buyer_id, $category, $description, $image, $starting_price, $reserve_price, $expiration, $stamp, $highest_bid, $recipient_id, $rating_score, $rating_feedback);

                        $auctions = array();

                        while ($dash_stmt->fetch()) {
                            $row = array('id' => $id, 'buyer' => $username, 'buyer_id' => $buyer_id, 'category' => $category, 'description' => stripslashes($description), 'image' => $image, 'starting_price' => $starting_price, 'reserve_price' => $reserve_price, 'expiration' => $expiration, 'stamp' => $stamp, 'highest_bid' => $highest_bid, 'recipient_id' => $recipient_id, 'rating_score' => $rating_score, 'rating_feedback' => $rating_feedback);
                            array_push($auctions, $row);
                        }
            
                        header('Content-Type: application/json', true, 200);
                        echo json_encode(array('auctions' => $auctions,'count' => $dash_cnt, 'q' => $q, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                    } else {
                        $res_code = $auc_cnt_stmt->errno < 2000 ? 400 : 500;

                        header('Content-Type: application/json', true, $res_code);
                        echo json_encode(array('error' => $dash_stmt->error, 'errtype' => 'dashboard', 'errno' => $dash_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                    }                   
                    $dash_stmt->close(); 
                }                
            }

            $this->db->close();            

        } else {
            BaseController::bad_request($method, $request);
        }  
    }

    /*
        DashboardController is used to show all of the auctions held or all of the auctions won for a particular user
    */


    // Gets all of the auctions won or held (depending on the parameter set in the GET request)
    public function show($method, $headers, $request) {

        /*
            The parameter in the GET request call is sold_or_won and possible values for this are either:
            1. sold
            2. won

            example request objects: GET request call (put the session-token in the header)
            /dashboard/show/?sold_or_won=sold
            /dashboard/show/?sold_or_won=won


            Sample response object for when sold_or_won= sold:
            {
              "auctions_held": [
                {
                  "auction_id": "2452241"
                },
                {
                  "auction_id": "2452242"
                }
              ]
            }

            Sample response object for when sold_or_won= won:
            {
              "auctions_won": [
                {
                  "auction_id": "2452241"
                },
                {
                  "auction_id": "2452242"
                }
              ]
            }
        */
        // buyer === 0, seller === 1, and combined === 2

        $json = file_get_contents('php://input');
        $data = json_decode($json, true);

        $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

        $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
        $user_stmt = $this->db->prepare("SELECT id, type FROM Users WHERE id = ?"); 
        
        $sess_stmt->bind_param('s', $sess_id);
        $user_stmt->bind_param('s', $user_key);
        
        $sess_id = $headers['session-token'];
        
        $sess_stmt->execute();
        $sess_stmt->bind_result($sess_id_db, $user_id);
        $sess_stmt->fetch();
        $sess_stmt->close();

        $user_key = $user_id;

        $user_stmt->execute();
        $user_stmt->bind_result($user_tbl_id, $user_type);
        $user_stmt->fetch();
        $user_stmt->close();


        if($sess_id_db === null){
            header('Content-Type: application/json', true, 401);
            echo json_encode(array('error' => 'Unauthorised session token.', 'errtype' => 'dashboard'));
        } else {
            if ($method === 'GET') {

                $sold_or_won = $this->db->escape_string($_GET['sold_or_won']);

                if ( ($user_type == 0) && ($sold_or_won==="sold")) {
                    header('Content-Type: application/json', true, 401);
                    echo json_encode(array('error' => 'Unauthorised to see the auctions held when you are a buyer.', 'errtype' => 'dashboard'));
                }
                else if ( ($user_type == 1) && ($sold_or_won==="won")) {
                    header('Content-Type: application/json', true, 401);
                    echo json_encode(array('error' => 'Unauthorised to see the auctions won when you are a seller.', 'errtype' => 'dashboard'));
                } else {
                    if ($sold_or_won==="sold"){

                        // Retrieve all of the auctions which the user has held
                        $stmt = $this->db->prepare("SELECT id AS auction_id FROM Auctions WHERE seller_id = ?");
                        $stmt->bind_param('s', $user_key);

                        if ($result=$stmt->execute()) {

                            $stmt->bind_result($auction_id);

                            $auctions_held = array();

                            while ($stmt->fetch()) {
                                $row = array('auction_id' => $auction_id);
                                array_push($auctions_held, $row);
                            }

                            $stmt->close();
                            $this->db->close();

                            header('Content-Type: application/json', true, 200);
                            echo json_encode(array('auctions_held' => $auctions_held));
                        } else {
                            header('Content-Type: application/json', true, 400);
                            echo json_encode(array('error' => 'Unable to get auctions that the user held ' . $headers['Session-Token']));            
                        }

                    } else if ($sold_or_won==="won"){

                        // Select all auctions from the auction table
                        $stmt = $this->db->prepare("SELECT id AS auction_id FROM Auctions");
                        $stmt->bind_param('s', $user_key);

                        if ($result=$stmt->execute()) {

                            $stmt->bind_result($auction_id);

                            // Iterate through all of the auctions and in each one, check if the user won it
                            $all_auctions = array();
                            while ($stmt->fetch()) {
                                array_push($all_auctions, $auction_id);  
                            }

                            $stmt->close();

                            $auctions_won = array();

                            foreach ($all_auctions as &$auction_id_iteration) {
                                $has_user_won_auction = $this->did_userid_win_auction($auction_id_iteration, $user_key, $json, $data);
                                if ($has_user_won_auction){
                                    $row = array('auction_id' => $auction_id_iteration);
                                    array_push($auctions_won, $row);  
                                }
                            }


                            $this->db->close();

                            header('Content-Type: application/json', true, 200);
                            echo json_encode(array('auctions_won' => $auctions_won));

                        }
                    }else {
                        header('Content-Type: application/json', true, 400);
                        echo json_encode(array('error' => 'You have to populate the variable sold_or_won in the GET request and the only values for this variable are won and sold.', 'errtype' => 'dashboard'));
                    }
                }
                
            } else {
                BaseController::bad_request($method, $request);
            }
        }
    }



// Checks whether $other_user_id won the bid in the auction
    function did_userid_win_auction($auction_id, $other_user_id, $json, $data){
        $buyer_id_stmt = $this->db->prepare("SELECT buyer_id, value FROM Bids, Auctions WHERE Bids.auction_id=Auctions.id AND Auctions.id=?");   
        $buyer_id_stmt->bind_param('s', $auction_id);
        $bidder_has_bidded_in_auction = false;

        if($bid_result=$buyer_id_stmt->execute()){

            $buyer_id_stmt->bind_result($buyer_id, $value);

            $bids = array();

            $max_value = 0;

            while ($buyer_id_stmt->fetch()){
                if ($value > $max_value) {
                    $max_value = $value;
                    $id_with_biggest_bid = $buyer_id;
                }
            }

            if ($id_with_biggest_bid != $other_user_id) {

                // The user in the parameter did not have the highest bid - not the winner
                $buyer_id_stmt->close();
                return false;
            } else {

                // The user in the parameter won the auction since they had the highest bid
                $buyer_id_stmt->close();
                return true;
            }
        }
    }


}
?>
