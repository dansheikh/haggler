<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class UsersController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function signup($method, $headers, $request) {
        if ($method === 'POST') {
            $json = file_get_contents('php://input');
            $data = json_decode($json, true);
            // Start transaction.
            $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
            $ins_stmt = $this->db->prepare("INSERT INTO Users(id, first_name, last_name, username, email, password, type) VALUES(?, ?, ?, ?, ?, ?, ?)");
            $ins_stmt->bind_param('sssssss', $user_id, $first_name, $last_name, $username, $email, $password, $type);

            $user_id = BaseController::generate_uuid();
            $first_name = $this->db->escape_string($data['first_name']);
            $last_name = $this->db->escape_string($data['last_name']);
            $username = $this->db->escape_string($data['username']);
            $email = $this->db->escape_string($data['email']);
            $password = hash('sha256', $data['password']);
            $type = $this->db->escape_string($data['type']);

            $ins_result = $ins_stmt->execute();

            if ($ins_result) {
                $sess_stmt = $this->db->prepare("INSERT INTO Sessions(id, user_id) VALUES(?, ?)");
                $sess_stmt->bind_param('ss', $sess_id, $user_id);

                $sess_id = BaseController::generate_uuid();
                $sess_result = $sess_stmt->execute();

                if ($sess_result) {
                    // Commit transaction.
                    $this->db->commit();
                    header('Content-Type: application/json', true, 201);
                    echo json_encode(array('session_token' => $sess_id, 'user_id' => $user_id, 'username' => $username, 'first_name' => $first_name, 'last_name' => $last_name, 'type' => intval($type)));
                } else {
                    $res_code = $sess_stmt->errno < 2000 ? 400 : 500;
                    $this->db->rollback();

                    header('Content-Type: application/json', true, $res_code);
                    echo json_encode(array('error' => $sess_stmt->error, 'errtype' => 'session', 'errno' => $sess_stmt->errno));
                }                 
                
                $sess_stmt->close();
            } else {
                $res_code = ($ins_stmt->errno < 2000 ? 400 : 500);
                $this->db->rollback();

                header('Content-Type: application/json', true, $res_code);
                echo json_encode(array('error' => $ins_stmt->error, 'errtype' => 'user', 'errno' => $ins_stmt->errno));
            }

            $ins_stmt->close();
            $this->db->close();

        } else {
            BaseController::bad_request($method, $request);
        }
    }
}
?>
