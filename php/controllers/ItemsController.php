<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class ItemsController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function create($method, $headers, $request) {
        /*
        example request object:
        {
            "description" : "an ancient book from the 19th century",
            "image" : "www.dropbox.com/sdgwswgwrgwrg/gwrgwrgwg/image1.png",
        }
        reponse:
        {
          "id": "c0d080ca-b843-4661-a1cf-9cc8e3f188be" //this is the id of the item that was created in the items table
        }
        */
        if ($method === 'POST') {
            $json = file_get_contents('php://input');
            $data = json_decode($json, true);

            $stmt = $this->db->prepare("INSERT INTO Items(id, description, image) VALUES(?, ?, ?)");
            $stmt->bind_param('sss', $id, $description, $image);

            $id = BaseController::generate_uuid();
            $description = $this->db->escape_string($data['description']);
            $image = $this->db->escape_string($data['image']);
            
            $result = $stmt->execute();
            $stmt->close();
            $this->db->close();

            if(!$result){
                echo header('Content-Type: application/json', true, 409);
                echo json_encode(array('error' => 'database query error'));
            }else{
                echo header('Content-Type: application/json', true, 201);
                echo json_encode(array('id' => $id));
            }
        } else {
            BaseController::bad_request($method, $request);
        }
    }

    
}
?>
