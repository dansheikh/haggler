<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class AuctionsController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function create($method, $headers, $request) {
        /*
            eg request object:
            {
                "category_id": "64437cbf-d8ad-11e5-a1bd-0242ac110001",
                "description": "Universal TV remote.",
                "image": "http://dropbox.com/path/to/image",
                "starting_price": 3.00,
                "reserve_price": 10.00,
                "expiration": "2016-02-10 02:02:10"
            }
        */

        // CHECK USER TYPE
        // uses session token info from header to check whether corresponding user is a buyer or a seller.
        // buyer === 0, seller === 1, and combined === 2
        $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

        $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
        $user_stmt = $this->db->prepare("SELECT id, type FROM Users WHERE id = ?"); 
        
        $sess_stmt->bind_param('s', $sess_id);
        $user_stmt->bind_param('s', $user_key);
        
        $sess_id = $headers['session-token'];
        
        $sess_stmt->execute();
        $sess_stmt->bind_result($sess_id_db, $user_id);
        $sess_stmt->fetch();
        $sess_stmt->close();

        $user_key = $user_id;

        $user_stmt->execute();
        $user_stmt->bind_result($user_tbl_id, $user_type);
        $user_stmt->fetch();
        $user_stmt->close();

        if($sess_id_db === null || $user_type < 1){
            header('Content-Type: application/json', true, 401);
            echo json_encode(array('error' => 'Unauthorised to create auction.', 'errtype' => 'auction'));
            return;
        }
        // END CHECK USER TYPE

        if ($method === 'POST') { 
            $json = file_get_contents('php://input');
            $data = json_decode($json, true);

            $item_stmt = $this->db->prepare("INSERT INTO Items(id, description, image) VALUES(?, ?, ?)");
            $item_stmt->bind_param('sss', $item_id, $desc, $img);

            $item_id = BaseController::generate_uuid();
            $desc = $this->db->escape_string($data['description']);
            $img = $this->db->escape_string($data['image']);

            $item_result = $item_stmt->execute();
            $item_stmt->close();

            if (!$item_result) {
                $res_code = $item_stmt->errno < 2000 ? 400 : 500;
                $this->db->rollback();

                header('Content-Type: application/json', true, $res_code);
                echo json_encode(array('error' => $item_stmt->error, 'errtype' => 'auction', 'errno' => $item_stmt->errno));
            }

            $auc_stmt = $this->db->prepare("INSERT INTO Auctions(id, seller_id, category_id, starting_price, reserve_price, item_id, stamp, expiration) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
            $auc_stmt->bind_param('ssssssss', $auc_id, $seller_id, $category_id, $starting_price, $reserve_price, $item_id, $stamp, $expiration);

            $auc_id = BaseController::generate_uuid();
            $seller_id =  $user_id;
            $category_id = $this->db->escape_string($data['category_id']);
            $starting_price = $this->db->escape_string($data['starting_price']);
            $reserve_price = $this->db->escape_string($data['reserve_price']);
            $expiration = $this->db->escape_string($data['expiration']);

            $auc_result = $auc_stmt->execute();            

            if (!$auc_result) {
                $res_code = $auc_stmt->errno < 2000 ? 400 : 500;
                $this->db->rollback();

                header('Content-Type: application/json', true, $res_code);
                echo json_encode(array('error' => $auc_stmt->error, 'errtype' => 'auction', 'errno' => $auc_stmt->errno));

                return;
            } else {
                $this->db->commit();
                echo header('Content-Type: application/json', true, 204);
            }

            $auc_stmt->close(); 
            $this->db->close();
        } else {
            BaseController::bad_request($method, $request);
        }
    }

    public function index($method, $headers, $request) {
        if ($method === 'GET') {
            $pg = array_key_exists('pg', $request) ? $request['pg'] : 1;

            $auc_cnt_stmt = $this->db->prepare("SELECT COUNT(id) FROM Auctions");
            $auc_cnt_stmt->bind_result($auc_cnt);
            $auc_cnt_res = $auc_cnt_stmt->execute();
            $auc_cnt_stmt->fetch();

            if ($auc_cnt_res && $auc_cnt === 0) {
                header('Content-Type: application/json', true, 200);
                echo json_encode(array('auctions' => array(), 'count' => $auc_cnt, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            } else if (!$auc_cnt_res) {
                $res_code = $auc_cnt_stmt->errno < 2000 ? 400 : 500;
                header('Content-Type: application/json', true, $res_code);
                echo json_encode(array('error' => $auc_cnt_stmt->error, 'errtype' => 'auction', 'errno' => $auc_cnt_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            } else {
                $auc_cnt_stmt->close();
                $auc_stmt = $this->db->prepare("SELECT a.id, c.title, i.description, i.image, a.starting_price, a.reserve_price, a.expiration, a.stamp, u.username, sq.avg_score FROM Auctions a JOIN Categories c ON a.category_id = c.id JOIN Items i ON a.item_id = i.id JOIN Users u ON a.seller_id = u.id LEFT JOIN (SELECT AVG(r.score) AS avg_score, r.recipient_id AS recipient_id FROM Ratings r GROUP BY r.recipient_id) sq ON a.seller_id = sq.recipient_id WHERE a.expiration > NOW() ORDER BY stamp DESC LIMIT ? OFFSET ?"); 
                $auc_stmt->bind_param('ss', self::$LIMIT, BaseController::offset($pg));
                $auc_result = $auc_stmt->execute();

                if ($auc_result) {
                    $auc_stmt->bind_result($id, $category, $description, $image, $starting_price, $reserve_price, $expiration, $stamp, $username, $avg_score);
                    $auctions = array();

                    while ($auc_stmt->fetch()) {
                        $row = array('id' => $id, 'category' => $category, 'description' => stripslashes($description), 'image' => $image, 'starting_price' => $starting_price, 'reserve_price' => $reserve_price, 'expiration' => $expiration, 'stamp' => $stamp, 'username' => $username, 'avg_score' => $avg_score);
                        array_push($auctions, $row);
                    }
        
                    header('Content-Type: application/json', true, 200);
                    echo json_encode(array('auctions' => $auctions,'count' => $auc_cnt, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                } else {
                    $res_code = $auc_stmt->errno < 2000 ? 400 : 500;
                    header('Content-Type: application/json', true, $res_code);
                    echo json_encode(array('error' => $auc_stmt->error, 'errtype' => 'auction', 'errno' => $auc_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                }

                $auc_stmt->close();
                $this->db->close();
            }

        } else {
            BaseController::bad_request($method, $request);
        }  
    }

    public function show($method, $headers, $request) {
        if ($method === 'GET') {
            $id = array_key_exists('id', $request) ? $request['id'] : NULL;

            if ($id === NULL) {
                echo header('Content-Type: application/json', true, 400);
                return;
            }

            $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

            $auc_stmt = $this->db->prepare("SELECT a.id, a.seller_id, seller.username AS seller, c.title, i.description, i.image, a.starting_price, a.reserve_price, a.expiration, a.stamp, a.views, buyer.username AS bidder, sq.buyer_id, sellsq.avg_score AS seller_rating, buysq.avg_score as buyer_rating, sq.value, sq.bid_stamp FROM Auctions a JOIN Users seller ON a.seller_id = seller.id JOIN Categories c ON a.category_id = c.id JOIN Items i ON a.item_id = i.id LEFT JOIN (SELECT b.auction_id, b.buyer_id AS buyer_id, b.value AS value, b.stamp AS bid_stamp FROM Bids b WHERE b.auction_id = ? ORDER BY b.value DESC LIMIT 1) sq ON a.id = sq.auction_id LEFT JOIN Users buyer ON sq.buyer_id = buyer.id LEFT JOIN (SELECT AVG(r.score) AS avg_score, r.recipient_id AS recipient_id FROM Ratings r GROUP BY r.recipient_id) sellsq ON a.seller_id = sellsq.recipient_id LEFT JOIN (SELECT AVG(r.score) AS avg_score, r.recipient_id AS recipient_id FROM Ratings r GROUP BY r.recipient_id) buysq ON sq.buyer_id = buysq.recipient_id WHERE a.id = ?");
            $auc_stmt->bind_param('ss', $id, $id);
            $auc_stmt->bind_result($id, $seller_id, $seller, $category, $description, $image, $starting_price, $reserve_price, $expiration, $stamp, $views, $bidder, $buyer_id, $seller_rating, $buyer_rating, $bid_value, $bid_stamp);

            if ($auc_stmt->execute()) {
                $auc_stmt->fetch();
                $auc_stmt->close();
            } else {                
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => $auc_stmt->error, 'errtype' => 'auction', 'errno' => $auc_stmt->errno));
                $auc_stmt->close();
                $this->db->rollback();
                return;
            }
        
            header('Content-Type: application/json', true, 200);
            echo json_encode(array('id' => $id, 'seller_id' => $seller_id, 'seller' => $seller, 'category' => $category, 'description' => stripslashes($description), 'image' => $image, 'starting_price' => $starting_price, 'reserve_price' => $reserve_price, 'expiration' => $expiration, 'bidder' => $bidder, 'buyer_id' => $buyer_id, 'seller_rating' => $seller_rating, 'buyer_rating' => $buyer_rating, 'bid_value' => $bid_value, 'bid_stamp' => $bid_stamp, 'stamp' => $stamp, 'views' => $views), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE); 

            $sess_stmt = $this->db->prepare("SELECT s.id, s.user_id FROM Sessions s WHERE id = ?");     
            $sess_stmt->bind_param('s', $sess_id);        
            $sess_id = $headers['session-token'];        

            if ($sess_stmt->execute()) {
                $sess_stmt->bind_result($sess_id_db, $user_id);
                $sess_stmt->fetch();
                $sess_stmt->close();                
            } else {
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => $sess_stmt->error, 'errtype' => 'auction', 'errno' => $sess_stmt->errno));
                $sess_stmt->close();
                $this->db->rollback();
                return;
            }

            $isActive = strtotime($expiration) > strtotime("now") ? true : false;

            if ($isActive && $user_id !== $seller_id) {
                $view_cnt = $views + 1;
                $view_cnt_stmt = $this->db->prepare("UPDATE Auctions SET views = ? WHERE id = ?");
                $view_cnt_stmt->bind_param('is', $view_cnt, $id);

                if ($view_cnt_stmt->execute()) {
                    $this->db->commit();                
                } else {
                    $this->db->rollback();
                }            

                $view_cnt_stmt->close();
            }

            $this->db->close();

        } else {
            BaseController::bad_request($method, $request);
        }
    }

    public function search($method, $headers, $request) {
        if ($method === 'GET') {
            $pg = array_key_exists('pg', $request) ? $request['pg'] : 1;
            $q = array_key_exists('q', $request) ? $request['q'] : NULL;
            $o = (array_key_exists('o', $request) && $request['o']) === "" ? 'DESC' : strtoupper($request['o']); // 0 === 'DESC', 1 === 'ASC'

            $auc_cnt_stmt = $this->db->prepare("SELECT COUNT(id) FROM Auctions a WHERE a.category_id = ? AND a.expiration > NOW()");
            $auc_cnt_stmt->bind_param('s', $q);
            $auc_cnt_stmt->bind_result($auc_cnt);
            $auc_cnt_res = $auc_cnt_stmt->execute();
            $auc_cnt_stmt->fetch();

            if ($auc_cnt_res && $auc_cnt === 0) {
                header('Content-Type: application/json', true, 200);
                echo json_encode(array('auctions' => array(), 'count' => $auc_cnt, 'pg' => $pg), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            } else if (!$auc_cnt_res) {
                $res_code = $auc_cnt_stmt->errno < 2000 ? 400 : 500;
                header('Content-Type: application/json', true, $res_code);
                echo json_encode(array('error' => $auc_cnt_stmt->error, 'errtype' => 'auction', 'errno' => $auc_cnt_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            } else {
                $auc_cnt_stmt->close();
                $sql = $o === 'DESC' ?  "SELECT a.id, c.title, i.description, i.image, a.starting_price, a.reserve_price, a.expiration, a.stamp, u.username, sq.avg_score FROM Auctions a JOIN Categories c ON a.category_id = c.id JOIN Items i ON a.item_id = i.id JOIN Users u ON a.seller_id = u.id LEFT JOIN (SELECT AVG(r.score) AS avg_score, r.recipient_id AS recipient_id FROM Ratings r GROUP BY r.recipient_id) sq ON a.seller_id = sq.recipient_id WHERE a.category_id = ? AND a.expiration > NOW() ORDER BY stamp DESC LIMIT ? OFFSET ?" : "SELECT a.id, c.title, i.description, i.image, a.starting_price, a.reserve_price, a.expiration, a.stamp, u.username, sq.avg_score FROM Auctions a JOIN Categories c ON a.category_id = c.id JOIN Items i ON a.item_id = i.id JOIN Users u ON a.seller_id = u.id LEFT JOIN (SELECT AVG(r.score) AS avg_score, r.recipient_id AS recipient_id FROM Ratings r GROUP BY r.recipient_id) sq ON a.seller_id = sq.recipient_id WHERE a.category_id = ? AND a.expiration > NOW() ORDER BY stamp ASC LIMIT ? OFFSET ?";
                $auc_stmt = $this->db->prepare($sql); 
                $auc_stmt->bind_param('sss', $q, self::$LIMIT, BaseController::offset($pg));                

                if ($auc_stmt->execute()) {
                    $auc_stmt->bind_result($id, $category, $description, $image, $starting_price, $reserve_price, $expiration, $stamp, $username, $avg_score);
                    $auctions = array();

                    while ($auc_stmt->fetch()) {
                        $row = array('id' => $id, 'category' => $category, 'description' => stripslashes($description), 'image' => $image, 'starting_price' => $starting_price, 'reserve_price' => $reserve_price, 'expiration' => $expiration, 'stamp' => $stamp, 'username' => $username, 'avg_score' => $avg_score);
                        array_push($auctions, $row);
                    }
        
                    header('Content-Type: application/json', true, 200);
                    echo json_encode(array('auctions' => $auctions,'count' => $auc_cnt, 'pg' => intval($pg)), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                } else {
                    $res_code = $auc_stmt->errno < 2000 ? 400 : 500;
                    header('Content-Type: application/json', true, $res_code);
                    echo json_encode(array('error' => $auc_stmt->error, 'errtype' => 'auction', 'errno' => $auc_stmt->errno), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
                }

                $auc_stmt->close();
                $this->db->close();
            }

        } else {
            BaseController::bad_request($method, $request);
        }  
    } 
}
?>
