<?php
namespace Haggler\Controllers;

require_once(dirname(__FILE__).'/BaseController.php');

class RatingsController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    /*
        Schema used for Ratings table:
            auction_id
            rater_id
            recipient_id
            feedback
            score
            stamp
    */


    // Checks whether $other_user_id won the bid in the auction
    function did_userid_win_auction($auction_id, $other_user_id, $json, $data){

        $buyer_id_stmt = $this->db->prepare("SELECT buyer_id, value FROM Bids, Auctions WHERE Bids.auction_id=Auctions.id AND Auctions.id=?");   
        $buyer_id_stmt->bind_param('s', $auction_id);
        $bidder_has_bidded_in_auction = false;

        if($bid_result=$buyer_id_stmt->execute()){

            $buyer_id_stmt->bind_result($buyer_id, $value);

            $bids = array();

            $max_value = 0;

            while ($buyer_id_stmt->fetch()){
                if ($value > $max_value) {
                    $max_value = $value;
                    $id_with_biggest_bid = $buyer_id;
                }
            }

            if ($id_with_biggest_bid != $other_user_id) {

                // The user in the parameter did not have the highest bid - not the winner
                $buyer_id_stmt->close();
                return false;
            } else {

                // The user in the parameter won the auction since they had the highest bid
                $buyer_id_stmt->close();
                return true;
            }
        }
    }

    // Helper function to insert the desired rating
    public function insert_to_ratings($user_id, $json, $data){
        $stmt = $this->db->prepare("INSERT INTO Ratings(auction_id, rater_id, recipient_id, score, feedback) VALUES(?, ?, ?, ?, ?)");
        $stmt->bind_param('sssis', $auction_id, $rater_id, $recipient_id, $score, $feedback);
        
        $auction_id = $this->db->escape_string($data['auction_id']);
        $rater_id = $user_id;
        $recipient_id = $this->db->escape_string($data['recipient_id']);
        $score = $this->db->escape_string($data['score']);
        $feedback = $this->db->escape_string($data['feedback']);

        $result = $stmt->execute();
        $stmt->close();
        $this->db->close();
        if(!$result){
            header('Content-Type: application/json', true, 409);
            echo json_encode(array('error' => 'database query error when inserting to ratings - cannot rate somebody more than once for the same auction'));
        }else{
            echo header('Content-Type: application/json', true, 204);
        }

    }


    public function create($method, $headers, $request) {
        /*
            eg request object: [populate the session-token in the header]
            {
               "auction_id" : "2d08f6e1-94c3-48a7-a7a8-e8d79dce39f4",
                "recipient_id" : "560caf9b-c1da-4762-ab01-0975ee47e0f9",
                "score": 2,
                "feedback": "my feedback"
            }
        */


        $json = file_get_contents('php://input');
        $data = json_decode($json, true);
        $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

        $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
        $user_stmt = $this->db->prepare("SELECT id, type FROM Users WHERE id = ?"); 
        
        $sess_stmt->bind_param('s', $sess_id);
        $user_stmt->bind_param('s', $user_key);
        
        $sess_id = $headers['session-token'];
        
        $sess_stmt->execute();
        $sess_stmt->bind_result($sess_id_db, $user_id);
        $sess_stmt->fetch();
        $sess_stmt->close();

        $user_key = $user_id;

        $user_stmt->execute();
        $user_stmt->bind_result($user_tbl_id, $user_type);
        $user_stmt->fetch();
        $user_stmt->close();

        if($sess_id_db === null || $user_key === null){
            header('Content-Type: application/json', true, 401);
            echo json_encode(array('error' => 'Unauthorised to create auction.', 'errtype' => 'auction'));
        } else {
            if ($method === 'POST') {

                // Check if the auction has expired yet. If it has not yet expired, then return an error
                $auction_expiration_stmt = $this->db->prepare("SELECT Auctions.expiration, Auctions.seller_id FROM Auctions WHERE Auctions.id=?"); 
                $auction_expiration_stmt->bind_param('s', $auction_id);
                
                $auction_id = $this->db->escape_string($data['auction_id']);
                $other_user_id = $this->db->escape_string($data['recipient_id']);

                if($auction_expiration_result=$auction_expiration_stmt->execute()){
                    $auction_expiration_stmt->bind_result($expiration, $seller_id_db);
                    $is_auction_expired = false;

                    if ($auction_expiration_stmt->fetch()) {
                        if(strtotime($expiration) - time() > 0) {
                            header('Content-Type: application/json', true, 403);
                            echo json_encode(array('error' => 'The auction has not yet expired, so you cannot rate this person.', 'errtype' => 'rating', 'errno' => null));
                            $auction_expiration_stmt->close();
                        } else {
                            $auction_expiration_stmt->close();
                            if ($user_type == 2) {
                                // Check if the user won the auction
                                if ($this->did_userid_win_auction($auction_id, $user_key, $json, $data)) {
                                    // User has won the auction - check other user to ensure they are the seller
                                    if ($seller_id_db == $other_user_id) {
                                        // Insert into ratings
                                        $this->insert_to_ratings($user_key, $json, $data);
                                    } else {
                                        header('Content-Type: application/json', true, 403);
                                        echo json_encode(array('Error' => 'The other user has to be the person holding the auction'));
                                    }
                                } else {
                                    // User is not the buyer, check if they are the seller
                                    if ($seller_id_db == $user_key) {
                                        // User is the seller, check if the other user won the auction
                                        if ($this->did_userid_win_auction($auction_id, $other_user_id, $json, $data)) {
                                            // Insert into ratings - User is the seller and other user won the auction
                                            $this->insert_to_ratings($user_key, $json, $data);
                                        } else {
                                            header('Content-Type: application/json', true, 403);
                                            echo json_encode(array('Error' => 'The other user has to win the auction'));
                                        }
                                    } else {
                                        header('Content-Type: application/json', true, 403);
                                        echo json_encode(array('Error' => 'The user did not win the auction nor did they hold it'));   
                                    }
         
                                }
                            } else if ($user_type == 1) {
                                // We know the user is a seller, check if they held this auction
                                if ($seller_id_db == $user_key) {
                                    // User is the seller for the auction, check if the other user won the auction
                                    if ($this->did_userid_win_auction($auction_id, $other_user_id, $json, $data)) {
                                        // Insert into ratings - User is the seller and other user won the auction
                                        $this->insert_to_ratings($user_key, $json, $data);
                                    } else {
                                        header('Content-Type: application/json', true, 403);
                                        echo json_encode(array('Error' => 'The other user has to win the auction'));
                                    }
                                } else {
                                    echo header('Content-Type: application/json', true, 403);
                                    echo json_encode(array('Error' => 'The user did not win the auction nor did they hold it'));   
                                }
                            } else if ($user_type == 0) {
                                // We know the user is a potential bidder, check if they won the auction
                                if ($this->did_userid_win_auction($auction_id, $user_key, $json, $data)) {
                                    // User has won the auction - check other user to ensure they are the seller
                                    if ($seller_id_db == $other_user_id) {
                                        // Insert into ratings
                                        $this->insert_to_ratings($user_key, $json, $data);
                                    } else {
                                        header('Content-Type: application/json', true, 403);
                                        echo json_encode(array('Error' => 'The other user has to be the person holding the auction'));
                                    }
                                } 
                            }
                        } 
                    } else{
                        header('Content-Type: application/json', true, 400);
                        echo json_encode(array('error' => 'Unable to get auction expiration date', 'errtype' => 'fetching expiration for auction'));   
                        $auction_expiration_stmt->close();
                        $this->db->close();
                    }
                }
            } else {
                BaseController::bad_request($method, $request);
            }
                
        }

    }           


    // Gets all of the ratings for a particular user
    public function show($method, $headers, $request) {

        /*
            eg request object: GET request call (put the session-token in the header)
            /ratings/show

            Sample response object:
            {
              "ratings": [
                {
                  "auction_id": "2d08f6e1-94c3-48a7-a7a8-e8d79dce39f4",
                  "rater_id": "e2f89d4d-51e2-470f-a702-4a6602942047",
                  "recipient_id": "3b47ad98-e9c2-49a0-ad2c-a5e8b0607a7f",
                  "feedback": "my feedback",
                  "score": 2,
                  "stamp": "2016-02-27 14:06:52"
                }
              ]
            }
     */

        $this->db->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

        $sess_stmt = $this->db->prepare("SELECT id, user_id FROM Sessions WHERE id = ?"); 
        $user_stmt = $this->db->prepare("SELECT id, type FROM Users WHERE id = ?"); 
        
        $sess_stmt->bind_param('s', $sess_id);
        $user_stmt->bind_param('s', $user_key);
        
        $sess_id = $headers['session-token'];
        
        $sess_stmt->execute();
        $sess_stmt->bind_result($sess_id_db, $user_id);
        $sess_stmt->fetch();
        $sess_stmt->close();

        $user_key = $user_id;

        $user_stmt->execute();
        $user_stmt->bind_result($user_tbl_id, $user_type);
        $user_stmt->fetch();
        $user_stmt->close();


        if($sess_id_db === null){
            header('Content-Type: application/json', true, 401);
            echo json_encode(array('error' => 'Unauthorised session token.', 'errtype' => 'auction'));
        } else {

            if ($method === 'GET') {
                $stmt = $this->db->prepare("SELECT * FROM Ratings WHERE recipient_id = ?");
                $stmt->bind_param('s', $user_key);
                if ($result=$stmt->execute()) {

                    $stmt->bind_result($auction_id, $rater_id, $recipient_id, $feedback, $score, $stamp);

                    $ratings = array();

                    while ($stmt->fetch()) {
                        $row = array('auction_id' => $auction_id, 'rater_id' => $rater_id, 'recipient_id' => $recipient_id, 'feedback' => $feedback, 'score' => $score, 'stamp' => $stamp);
                        array_push($ratings, $row);
                    }

                    $stmt->close();
                    $this->db->close();

                    header('Content-Type: application/json', true, 200);
                    echo json_encode(array('ratings' => $ratings));
                } else {
                    header('Content-Type: application/json', true, 400);
                    echo json_encode(array('error' => 'Unable to get user ratings ' . $headers['Session-Token']));            
                }
                
            } else {
                BaseController::bad_request($method, $request);
            }
        }
    }




    public function index($method, $headers, $request) {

        /*
            eg request object: GET request call:
            /ratings/index

            Sample response object:
            {
              "ratings": [
                {
                  "auction_id": "2d08f6e1-94c3-48a7-a7a8-e8d79dce39f4",
                  "rater_id": "e2f89d4d-51e2-470f-a702-4a6602942047",
                  "recipient_id": "3b47ad98-e9c2-49a0-ad2c-a5e8b0607a7f",
                  "feedback": "my feedback",
                  "score": 2,
                  "stamp": "2016-02-27 14:06:52"
                }
              ]
            }   
     */

        if ($method === 'GET') {
            $stmt = $this->db->prepare("SELECT * FROM Ratings");
            if ($result=$stmt->execute()) {

                $stmt->bind_result($auction_id, $rater_id, $recipient_id, $feedback, $score, $stamp);

                $ratings = array();

                while ($stmt->fetch()) {
                    $row = array('auction_id' => $auction_id, 'rater_id' => $rater_id, 'recipient_id' => $recipient_id, 'feedback' => $feedback, 'score' => $score, 'stamp' => $stamp);
                    array_push($ratings, $row);
                }

                $stmt->close();
                $this->db->close();

                header('Content-Type: application/json', true, 200);
                echo json_encode(array('ratings' => $ratings));
            } else {
                header('Content-Type: application/json', true, 400);
                echo json_encode(array('error' => 'Unable to delete session ' . $headers['Session-Token']));            
            }
            
        } else {
            BaseController::bad_request($method, $request);
        }
    }

}
?>
