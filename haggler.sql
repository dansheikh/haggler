drop database if exists Haggler;
create database Haggler;

use Haggler;

create table Users(
    id varchar(36) not null,
    first_name char(255) not null,
    last_name char(255) not null,
    username char(255) not null,
    email varchar(255) not null,
    password varchar(255) not null,
    type tinyint(1) not null,
    primary key (id),
    constraint uq_username unique(username),
    constraint uq_email unique (email)
);

create table Categories(
    id varchar(36) not null,
    title varchar(255) not null,
    primary key (id)
);
 
create table Auctions(
    id varchar(36) not null,
    seller_id varchar(36) not null,
    category_id varchar(36) not null,
    item_id varchar(36) not null,
    starting_price decimal(12,2) not null,
    reserve_price decimal(12,2) not null,
    expiration timestamp not null default now(),
    stamp timestamp not null default now(),
    views int not null default 0,
    primary key (id),
    constraint chk_expiration check (expiration > now()),
    constraint fk_user_auction foreign key (seller_id) references Users(id) on delete cascade,
    constraint fk_category_auction foreign key (category_id) references Categories(id) on delete cascade,
    constraint fk_item_auction foreign key (item_id) references Items(id) on delete cascade
);

create table Items(
    id varchar(36) not null,
    description varchar(255) not null,
    image varchar(255) null,
    primary key (id)
);
 
create table Ratings(
    auction_id varchar(36) not null,
    rater_id varchar(36) not null,
    recipient_id varchar(36) not null,
    feedback varchar(255) null,
    score tinyint(1) not null default 0,
    primary key (auction_id, rater_id, recipient_id),
    stamp timestamp not null default now(),
    constraint fk_rater_rating foreign key (rater_id) references Users(id) on delete cascade,
    constraint fk_recipient_rating foreign key (recipient_id) references Users(id) on delete cascade,
    constraint fk_auction_rating foreign key (auction_id) references Auctions(id) on delete cascade
);

create table Bids(
    id varchar(36) not null,
    buyer_id varchar(36) not null,
    auction_id varchar(36) not null,
    value decimal(12,2) not null,
    stamp timestamp not null default now(),
    primary key (id),
    constraint fk_user_bid foreign key (buyer_id) references Users(id) on delete cascade,
    constraint fk_auction_bid foreign key (auction_id) references Auctions(id) on delete cascade
);

create table Observers(
    buyer_id varchar(36) not null,
    auction_id varchar(36) not null,
    stamp timestamp not null default now(),
    primary key (buyer_id, auction_id),
    constraint fk_user_observer foreign key (buyer_id) references Users(id),
    constraint fk_auction_observer foreign key (auction_id) references Auctions(id) on delete cascade
);

create table Sessions(
    id varchar(36) not null,
    user_id varchar(36) not null,
    stamp timestamp not null default now(),
    primary key (id),
    constraint fk_user_session foreign key (user_id) references Users(id) on delete cascade
);

create table Notifications(
    id varchar(36) not null,
    user_id varchar(36) not null,
    auction_id varchar(36) not null,
    type tinyint(1) not null, # {  -2: 'auction lost', -1: 'auction expired', 0: 'outbid', 1: 'purchase', 2: 'sale', 3: 'payment' }
    seen tinyint(1) not null default 0,
    stamp timestamp not null default now(),
    primary key (id),
    constraint fk_user_note foreign key (user_id) references Users(id) on delete cascade,
    constraint fk_auction_note foreign key (auction_id) references Auctions(id) on delete cascade
);

delimiter $$
create trigger expiration_check before insert on Auctions for each row
    begin
        if (new.expiration = now()) then
            set new.expiration = DATE_ADD(now(), interval 1 day);
        end if;
    end$$
delimiter ;

grant select, insert, update, delete on Haggler.* to 'bargainhunter'@'%' identified by 'thrifty';
grant select, insert, update, delete on Haggler.* to 'bargainhunter'@'localhost' identified by 'thrifty';

insert into Categories(id, title) 
            VALUES(uuid(), "Electronics"),
            (uuid(), "Publications"),
            (uuid(), "Entertainment"),
            (uuid(), "Vehicles"),
            (uuid(), "Tickets"),
            (uuid(), "Professional Services"),
            (uuid(), "Personal Services");
