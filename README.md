# Haggler #

## Setup ##

### Apache ###

1. Add the following virtual host configuration to your local Apache configuration file (httpd.conf).

```
#!xml

NameVirtualHost *:80
<VirtualHost *:80>
        DocumentRoot /var/www/php
        ServerName localhost
        EnableSendfile Off
        <Directory "/var/www/php">
                Options Indexes FollowSymLinks
                AllowOverride All
                Allow from all
        </Directory>
        <IfModule mod_rewrite.c>
                RewriteEngine On

                RewriteCond %{REQUEST_METHOD} OPTIONS
                RewriteRule ^(.*)$ /options.php?url=$1 [QSA,L]

                #RewriteCond %{REQUEST_FILENAME} !-d
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteRule ^(.*)$ /main.php?url=$1 [QSA,PT,L]
        </IfModule>
</VirtualHost>
```

### MySQL/MariaDB ###

1. Install MySQL/MariaDB and configure root user password.
2. Execute haggler.sql script.

```
#!bash

mysql -u root -p < /path/to/haggler.sql
```

3. Create a *db.json* file within the *php/configurations* directory and add the following content. Change the host value according to local setup.

```
#!json

{
    "host": "p:127.0.0.1",
    "user": "bargainhunter",
    "password": "thrifty",
    "db": "Haggler"
}
```
4. Create a *email.json* file within the *php/configurations* directory and add the following content. Change the following values according to local setup.
```
#!json

{
    "email": "bargainhunter@domain.com",
    "password": "thrifty",
    "stmp_host": "ssl://smtp.domain.com",
    "port": 465
}
```

### Contribution Guidelines ###

* Clone repository
* Create _new_ branch for each feature or fix
* Push _new_ branch to origin
* Create pull request -- remember to select "close branch" after pull option -- for peer review and acceptance 

*Note: pull requests must be reviewed by a teammate, excluding pull request author.*
